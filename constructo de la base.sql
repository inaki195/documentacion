-- creacion de base de datos
-- create database gestionfisio;
-- use gestionfisio;
-- creacion de tablas
create table usuarios(
id int primary key auto_increment,
nombre varchar(60),
apellidos varchar(120),
dni varchar(10),
tipo enum('cliente','fisio'),
nickname varchar(100),
correo varchar(120),
passwd varchar(90),
fech_nac date
);
create table clientes(
id int primary key ,
nombre varchar(60),
apellidos varchar(120),
dni varchar(10),
tipo enum('cliente','fisio'),
nickname varchar(100),
correo varchar(120),
passwd varchar(90),
fech_nac date
);
create table fisios(
id int primary key ,
estudios varchar(100),
reputacion enum('mala','regular','buena'),
rango enum('admin','trabajador'),
nombre varchar(60),
apellidos varchar(120),
dni varchar(10),
tipo enum('cliente','fisio'),
nickname varchar(100),
correo varchar(120),
passwd varchar(90),
fech_nac date
);


create table credenciales(
id int primary key,
user varchar(100),
passwd varchar(100)
);


-- claves foraneas
-- ids con diferentes usuarios
alter table clientes
add foreign key(id) references usuarios(id);
alter table fisios
add foreign key(id)references usuarios(id);
alter table credenciales
add foreign key(id)references usuarios(id);

--  claves foraneas en las incidencias
alter table  incidencias_clientes 
add foreign key(id_incidencia) references incidencias(id);
alter table  incidencias_clientes 
add foreign key(id_cliente) references clientes(id);

