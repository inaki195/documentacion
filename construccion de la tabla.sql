-- creacion de base de datos
create database gestionfisio;
use gestionfisio;
-- creacion de tablas
create table incidencias(
id int primary key auto_increment,
molestia varchar(100),
nivel_dolor enum('leve','moderado','grave'),
reicidente boolean ,
estado enum ('pendiente','en proceso','aceptado','rechazado'),
id_cliente int
);
create table usuarios(
id int primary key auto_increment,
nombre varchar(60),
apellidos varchar(120),
dni varchar(10),
tipo enum('cliente','fisio'),
nickname varchar(100),
correo varchar(120),
passwd varchar(90),
fech_nac date
);

create table citas(
id int primary key auto_increment,
id_fisio int ,
id_cliente int,
precio float,
lugar varchar(120),
duracion time,
fecha date
);
create table clientes(
id_cliente int primary key auto_increment,
nombre varchar(60),
apellidos varchar(120),
dni varchar(10),
tipo enum('cliente','fisio'),
nickname varchar(100),
correo varchar(120),
passwd varchar(90),
fech_nac date
);
create table fisios(
id_fisio int primary key auto_increment,
estudios varchar(100),
reputacion enum('mala','regular','buena'),
rango enum('admin','trabajador'),
nombre varchar(60),
apellidos varchar(120),
dni varchar(10),
tipo enum('cliente','fisio'),
nickname varchar(100),
correo varchar(120),
passwd varchar(90),
fech_nac date
);
create table credenciales(
user varchar(50),
passwd varchar(50)
);
-- claves foareneas
alter table incidencias
add foreign key(id_cliente) references clientes(id_cliente);
alter table citas
add foreign key (id_fisio) references fisios(id_fisio);
alter table citas
add foreign key(id_cliente) references clientes(id_cliente);