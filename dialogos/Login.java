package hibernate.gui.vista;

import java.awt.GridBagConstraints;
import java.time.temporal.JulianFields;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Login extends JDialog {
	JPanel panelLogin;
	JLabel labelLoginNick;
	JLabel labelLoginPasswd;
	JTextField tfLoginnick;
	JTextField tfLoginpasswd;
	JButton btnLoginAccoutInit;
	JButton btnLoginAccoutRegistrer;
	GridBagConstraints ggc;
	/*
	 * inicializador
	 */
	public Login() {
		super();
		panelLogin=new JPanel();
		labelLoginNick=new JLabel("    usuario :");
		labelLoginPasswd=new JLabel("contraseņa :");
		tfLoginnick=new JTextField(15);
		tfLoginpasswd=new JTextField(15);
		btnLoginAccoutInit=new JButton("iniciar cesion");
		btnLoginAccoutRegistrer=new JButton("crear cuenta");
		
		ggc=new GridBagConstraints();
		//dar forma
		setTitle("login");
		setSize(300,200);
		//metodo que no permite una redimension
		setResizable(false);
		setContentPane(panelLogin);
		organizador(0, 0);
	    panelLogin.add(labelLoginNick,ggc);
	    organizador(1, 0);
	   // ggc.anchor=3;
	    panelLogin.add(tfLoginnick,ggc);
	    organizador(0, 1);
	    panelLogin.add(labelLoginPasswd,ggc);
	    organizador(1, 1);
	    ggc.fill=5;
	    panelLogin.add(tfLoginpasswd,ggc);
	
	   organizador(2, 0);
	   panelLogin.add(btnLoginAccoutInit,ggc);
	   organizador(2, 1);
	   panelLogin.add(btnLoginAccoutRegistrer,ggc);
	    
	
		/**
		 * 	
		 * organizador(0, 13);
		panelLogin.add(tfLoginpasswd,ggc);
		organizador(1, 13);
		 * 
		 */
		
		setVisible(true);
		
		
	}
	/**
	 * organiza la posicion del {@link GridBagConstraints}
	 * @param x eje horizontal
	 * @param y eje vertical
	 */
	private void organizador(int x, int y) {
		ggc.gridx=x;
		ggc.gridy=y;		
		
	}
	
	
	
}
