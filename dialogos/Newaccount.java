package hibernate.gui.vista;

import java.awt.GridBagConstraints;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;
import hibernate.gui.vista.Util;
public class Newaccount extends JDialog {
	private JPanel pn_account;
	private JLabel lbl_ac_name;
	private JTextField txt_ac_nombre;
	private JLabel lbl_ac_usuario;
	private JLabel lbl_ac_apellido;
	private JLabel lbl_ac_correo;
	private JLabel lbl_ac_dni;
	private JLabel lbl_ac_nick;
	private JLabel lbl_ac_passwd;
	private JLabel lbl_ac_fecha;
	private JTextField txt_ac_apellido;
	private JTextField txt_ac_dni;
	private JTextField txt_ac_nick;
	private JTextField txt_ac_passwd;
	private JTextField txt_ac_correo;
	private JRadioButton rb_ac_fisio;
	private JRadioButton rb_ac_cliente;
	private ButtonGroup rbg_ac;
	JTextField tfLoginnick;
	JTextField tfLoginpasswd;
	JButton btn_ac_init;
	GridBagConstraints ggc;
	public Newaccount() {
	
		super();

		pn_account=new JPanel();
		setContentPane(pn_account);
		ggc=new GridBagConstraints();
		iniciarlizartx();
		inicializarLB();
		inicializarRB();
		posicion();
	
	}
	
	private void posicion() {
		organizador(0, 0);
		pn_account.add(lbl_ac_name,ggc);
		organizador(1, 0);
		pn_account.add(txt_ac_nombre,ggc);
		organizador(1, 2);
		pn_account.add(lbl_ac_apellido,ggc);
		organizador(2, 2);
		pn_account.add(txt_ac_apellido,ggc);
		organizador(1, 3);
		pn_account.add(lbl_ac_dni,ggc);
		organizador(2, 3);
		pn_account.add(txt_ac_dni,ggc);
		organizador(1, 4);
		pn_account.add(lbl_ac_nick,ggc);
		organizador(2, 4);
		pn_account.add(txt_ac_nick,ggc);
		organizador(3, 4);
		pn_account.add(lbl_ac_passwd,ggc);
		organizador(4, 4);
		pn_account.add(txt_ac_passwd,ggc);
		organizador(1, 5);
		pn_account.add(lbl_ac_correo,ggc);
		organizador(2, 5);
		pn_account.add(txt_ac_correo,ggc);
		organizador(1, 6);
		pn_account.add(rb_ac_cliente,ggc);
		organizador(2, 6);
		pn_account.add(rb_ac_fisio,ggc);
		organizador(1, 7);
		pn_account.add(btn_ac_init,ggc);
		setTitle("registro de usuario");
		setSize(250,300);
		setResizable(false);
		setVisible(true);
		
	}

	private void inicializarRB() {
		rb_ac_cliente=new JRadioButton("cliente ");
		rb_ac_fisio=new JRadioButton(" fisioterapeuta");
		rbg_ac=new ButtonGroup();
		rbg_ac.add(rb_ac_cliente);
		rbg_ac.add(rb_ac_fisio);
		btn_ac_init=new JButton("aceptar");
	}

	private void iniciarlizartx() {
		txt_ac_nombre=new JTextField(10);
		txt_ac_apellido=new JTextField(10);
		txt_ac_dni=new JTextField(10);
		txt_ac_nick=new JTextField(10);
		txt_ac_passwd=new JTextField(10);
		txt_ac_correo=new JTextField(10);
	}

	private void inicializarLB() {
		lbl_ac_name=new JLabel("   nombre :");
		lbl_ac_apellido=new JLabel("   apellidos :");
		lbl_ac_correo=new JLabel("  correo");
		lbl_ac_dni=new JLabel("numero de DNI");
		lbl_ac_nick=new JLabel("nombre de usuario");
		lbl_ac_passwd=new JLabel("contraseņa");
		lbl_ac_fecha=new JLabel("fecha de nacimiento");
		
	}

	/**
	 * organiza la posicion del {@link GridBagConstraints}
	 * @param x eje horizontal
	 * @param y eje vertical
	 */
	private void organizador(int x, int y) {
		ggc.gridx=x;
		ggc.gridy=y;
	}
	
}
