package hibernate.gui.vista;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.time.temporal.JulianFields;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
public class Salir extends JDialog {

		JPanel panelLogin;
		JLabel lbl_msg;
		JLabel lbl_advertencia;
		JLabel lbl_empty;
		
		JButton btn_ok;
		JButton btn_cancel;
		GridBagConstraints ggc;
		/*
		 * inicializador
		 */
		public Salir() {
			super();
			panelLogin=new JPanel();
			lbl_empty=new JLabel(" ?");
			lbl_msg=new JLabel("�   Estas seguro de que quieres salir");
			lbl_advertencia=new JLabel("");
			btn_cancel=new JButton("cancelar");
			btn_ok=new JButton("salir");
			setContentPane(panelLogin);
			ggc=new GridBagConstraints();
			//dar forma
			setTitle("salir");
			setSize(300,200);
			//metodo que no permite una redimension
			setResizable(false);
			
			organizador(0,0);
		    panelLogin.add(lbl_msg,ggc);
		    organizador(0, 1);
		    panelLogin.add(lbl_empty,ggc);
			organizador(1, 1);
			panelLogin.add(btn_ok,ggc);
			organizador(1, 2);
			panelLogin.add(btn_cancel,ggc);
			organizador(2, 0);
			panelLogin.add(lbl_advertencia,ggc);
			
	
			/**
			 * 	
			 * organizador(0, 13);
			panelLogin.add(tfLoginpasswd,ggc);
			organizador(1, 13);
			 * 
			 */
			
			setVisible(true);
			
			
		}
		/**
		 * organiza la posicion del {@link GridBagConstraints}
		 * @param x eje horizontal
		 * @param y eje vertical
		 */
		private void organizador(int x, int y) {
			ggc.gridx=x;
			ggc.gridy=y;		
			
		}
		
}
