-- creacion de base de datos
create database gestionfisio;
  use gestionfisio;
-- create database gestionfisio;
-- use gestionfisio;
-- creacion de tablas
create table incidencias(
id int primary key auto_increment,
molestia varchar(225),
nivel_dolor enum('leve','moderado','grave'),
reincidente boolean,
estado enum ('pendiente','en proceso','aceptado','rechazado')
);
create table usuarios(
id int primary key auto_increment,
nombre varchar(60),
apellidos varchar(120),
dni varchar(10),
tipo enum('cliente','fisio'),
nickname varchar(100),
correo varchar(120),
passwd varchar(90),
fech_nac date
);
create table clientes(
id int primary key auto_increment,
id_usuario int,
nombre varchar(60),
apellidos varchar(120),
dni varchar(10),
nickname varchar(100),
correo varchar(120),
passwd varchar(90),
fech_nac date
);
create table fisios(
id int primary key auto_increment,
id_usuario int ,
estudios varchar(100),
reputacion enum('mala','regular','buena'),
rango enum('admin','trabajador'),
nombre varchar(60),
apellidos varchar(120),
dni varchar(10),
nickname varchar(100),
correo varchar(120),
passwd varchar(90),
fech_nac date
);
create table citas(
id int primary key auto_increment,
id_cliente int,
id_fisio int,
precio float,
lugar varchar(120),
duracion time,
fecha date
);

create table credenciales(
id int primary key auto_increment,
id_usuario int,
user varchar(100),
passwd varchar(100)
);


create table cliente_incidencia(
id_cliente int,
id_incidencia int
);
-- claves foraneas dolencias y clientes
alter table  cliente_incidencia
add foreign key (id_cliente) references clientes(id);
alter table  cliente_incidencia
add foreign key (id_incidencia) references incidencias(id);
-- claves foraneas citas y fisios
alter table  citas
add foreign key (id_cliente) references clientes(id);
alter table  citas
add foreign key (id_fisio) references fisios(id);

-- ids con diferentes usuarios
alter table clientes
add foreign key(id_usuario) references usuarios(id);
alter table fisios
add foreign key(id_usuario)references usuarios(id);
alter table credenciales
add foreign key(id_usuario)references usuarios(id);



